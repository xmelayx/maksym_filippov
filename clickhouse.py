import docker
import time
import subprocess
from argparse import ArgumentParser

ap = ArgumentParser(description="Account creater")
ap.add_argument('-bn', '--backup_name', dest='backup_name', type=str, required=True,
                help='Backup name')
ap.add_argument('-sp', '--s3_path', dest='s3_path', type=str, required=True,
                help='Username: tester')
args = ap.parse_args()

class Docker():

    client = docker.from_env()
    clickhouse_satus = ""
    backup_name = args.backup_name
    s3_path = args.s3_path

    def docker_run_clickhouse(self):
        container = self.client.containers.run(image="flexberry/clickhouse-official",
                                               name="clickhouse",detach=True, ports={9000:9001},
                                               volumes={"clickhouse": {'bind': '/var/lib/clickhouse', 'mode': 'rw'}})
        time.sleep(5)
        self.clickhouse_satus = container.status
    def dockcer_run_test_backup(self):
        try:
            self.docker_run_clickhouse()
        except:
            self.clickhouse_satus = "created"
            print("clickhouse container already started")
        if self.clickhouse_satus != "created":
            print("Error start clickhouse container")
        env = {
            "CLICKHOUSE_PASSWORD": "",
            "S3_PATH": self.s3_path,
            "CLICKHOUSE_DATA_PATH": "/Users/m_filippov/maxi/clickhouse",
            "CLICKHOUSE_HOST": "localhost",
            "CLICKHOUSE_PORT": 9000
            }
        container = self.client.containers.run(image="alexakulov/clickhouse-backup:master",
                                          name="tests", detach=False, environment=env, remove=True,
                                          network_mode="host", command=f"restore {self.backup_name}",
                                          volumes={f"{self.s3_path}": {'bind': f'{self.s3_path}', 'mode': 'rw'}} )
        print(container.logs())
if __name__ == '__main__':
    args = ap.parse_args()
    Docker.dockcer_run_test_backup(Docker)
